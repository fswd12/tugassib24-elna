document.addEventListener("DOMContentLoaded", function () {
    const slides = document.querySelectorAll('.carousel img');
    const totalSlides = slides.length;
    let currentIndex = 0;

    // Function to show a particular slide
    function showSlide(index) {
        slides.forEach((slide) => {
            slide.style.transition = 'transform 0.5s ease';
            slide.style.transform = `translateX(-${index * 100}%)`;
        });
    }

    // Function to show the next slide
    function nextSlide() {
        currentIndex = (currentIndex + 1) % totalSlides;
        showSlide(currentIndex);
        updateIndicators();
    }

    // Function to show the previous slide
    function prevSlide() {
        currentIndex = (currentIndex - 1 + totalSlides) % totalSlides;
        showSlide(currentIndex);
        updateIndicators();
    }

    // Function to update the indicators based on the current slide
    function updateIndicators() {
        const indicators = document.querySelectorAll(".carousel-indicators .indicator");
        indicators.forEach((indicator, index) => {
            if (index === currentIndex) {
                indicator.classList.add("active");
            } else {
                indicator.classList.remove("active");
            }
        });
    }

    // Auto advance slides every 3 seconds
    setInterval(nextSlide, 3000);

    // Add indicators for each slide
    const indicatorContainer = document.querySelector(".carousel-indicators");
    for (let i = 0; i < totalSlides; i++) {
        const indicator = document.createElement("div");
        indicator.classList.add("indicator");
        indicatorContainer.appendChild(indicator);
    }

    // Add event listener for clicking on indicators
    const indicators = document.querySelectorAll(".carousel-indicators .indicator");
    indicators.forEach((indicator, index) => {
        indicator.addEventListener("click", () => {
            currentIndex = index;
            showSlide(currentIndex);
            updateIndicators();
        });
    });

    // Show the first slide and update indicators initially
    showSlide(currentIndex);
    updateIndicators();
});
